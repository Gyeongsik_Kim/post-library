/**
* date : 2017 - 07 - 19
* author : Gyeongsik Kim
**/

const express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json());

app.all('/', function(req, res){
	console.log(res.body.company);
	const json = res.body;
	var result = JSON.parse('{}');
	result['version'] = 1;
	result['orignal_request'] = json;
	res.header("Content-Type", "application/json; charset=utf-8");
	if(typeof json["company"] != undefined) {
		const lib = require('./post_lib.js');
		result['value'] = lib.tracking(json['company'], json['code']);
	}else{
		result['error'] = true;
		result['description'] = 'Not found company';
	}
	res.send(result);
});

var server = app.listen(12345, function () {
    console.log("Express server has started on port 12345")
})