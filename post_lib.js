const version = 1;

exports.tracking = function (company, code) {
	var result = JSON.parse('{}');
	result['version'] = version;
	const companyInfo = findCompany(company);
	if(companyInfo['error'] != false){
		result['error'] = companyInfo['error'];
		result['description'] = companyInfo['description'];
		return result;
	}else{
		companyInfo['value']['url'] = companyInfo['value']['url'].replace('#CODE#', code);
		console.log(companyInfo['value']);
		const request = require('request');
		request.post({
			headers: {'content-type' : 'application/x-www-form-urlencoded'},
			url : companyInfo['value']['url'],
			body: ('billno=' + code + '&sf=')
		}, function(error, response, body){
			const cheerio = require('cheerio');  
			const $ = cheerio.load(body);		
			console.log(body);
			// var postElements = $("body > div.content-wrapper > article > section > div");
			// postElements.each(function() {
				// if(temp > 2)
					// return;
				// temp = temp + 1;
				// const post_title = $(this).find("h4 a").text().trim();
				// const post_url = $(this).find("h4 a").attr('href');
				// const post_thumb = $(this).find("div").text();
				// console.log('title : ' + post_title);
				// console.log('url : ' + post_url);
				// console.log('thumb : ' + post_thumb);
				
				// message.channel.send(`==== TITLE : ${post_title} ====\n\n` + '```\n' +
						// `${post_thumb}...` + '```\n' + `\n\nhttps://namu.wiki${post_url}\n`);
			// });
		});
	}
};


function findCompany(company){
	const companyList = [
		{'name':'경동택배', 'url':'http://www.kdexp.com/sub3_shipping.asp?stype=1&p_item=#CODE#'},
		{'name':'대신택배', 'url':'https://www.ds3211.co.kr/freight/internalFreightSearch.ht'},
		{'name':'대한항공', 'url':'http://cargo.koreanair.com/ecus/trc/servlet/TrackingServlet?pid=5&version=kor&menu1=m1&menu2=m01-1&awb_no=#CODE#'},
		{'name':'동부택배', 'url':'http://www.dongbups.com/newHtml/delivery/delivery_search_view.jsp?item_no=#CODE#'},
		{'name':'로젠택배', 'url':'http://www.ilogen.com/iLOGEN.Web.New/TRACE/TraceView.aspx?gubun=slipno&slipno=#CODE#'},
		{'name':'범한판토스', 'url':'http://www.epantos.com/jsp/gx/tracking/tracking/trackingInquery.jsp?refNo=#CODE#'},
		{'name':'우체국택배', 'url':'http://service.epost.go.kr/trace.RetrieveRegiPrclDeliv.postal?sid1=#CODE#'},
		{'name':'일양로지스택배', 'url':'http://www.ilyanglogis.com/functionality/tracking_result.asp?hawb_no=#CODE#'},
		{'name':'천일택배', 'url':'http://www.chunil.co.kr/HTrace/HTrace.jsp?transNo=#CODE#'},
		{'name':'한덱스택배', 'url':'http://btob.sedex.co.kr/work/app/tm/tmtr01/tmtr01_s4.jsp?IC_INV_NO=#CODE#'},
		{'name':'한의사랑택배', 'url':'http://www.hanips.com/html/sub03_03_1.html?logicnum=#CODE#'},
		{'name':'한진택배', 'url':'http://www.hanjin.co.kr/Delivery_html/inquiry/result_waybill.jsp?wbl_num=#CODE#'},
		{'name':'현대택배', 'url':'http://www.hlc.co.kr/personalService/tracking/06/tracking_goods_result.jsp?InvNo=#CODE#'},
		{'name':'CJ 대한통운', 'url':'https://www.doortodoor.co.kr/parcel/doortodoor.do?fsp_action=PARC_ACT_002&fsp_cmd=retrieveInvNoACT&invc_no=#CODE#'},
		{'name':'CVSnet편의점택배', 'url':'http://was.cvsnet.co.kr/_ver2/board/ctod_status.jsp?invoice_no=#CODE#'},
		{'name':'DHL택배', 'url':'http://www.dhl.co.kr/content/kr/ko/express/tracking.shtml?brand=DHL&AWB=#CODE#'},
		{'name':'FedEx택배', 'url':'http://www.fedex.com/Tracking?ascend_header=1&clienttype=dotcomreg&cntry_code=kr&language=korean&tracknumbers=#CODE#'},
		{'name':'GTX로지스택배', 'url':'http://www.gtxlogis.co.kr/tracking/default.asp?awblno=#CODE#'},
		{'name':'KG옐로우캡택배', 'url':'http://www.yellowcap.co.kr/custom/inquiry_result.asp?INVOICE_NO=#CODE#'},
		{'name':'KGB택배', 'url':'http://www.kgbls.co.kr/sub5/trace.asp?f_slipno=#CODE#'},
		{'name':'OCS택배', 'url':'http://www.ocskorea.com/online_bl_multi.asp?mode=search&search_no=#CODE#'},
		{'name':'TNT Express', 'url':'http://www.tnt.com/webtracker/tracking.do?respCountry=kr&respLang=ko&searchType=CON&cons=#CODE#'},
		{'name':'UPS택배', 'url':'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=#CODE#'}
	];
	var json = JSON.parse('{}');
	if(company.trim() == '택배'){
		json['error'] = true;
		json['description'] = 'Input value catch';
		return json;
	}
	for(var index in companyList){
		if(companyList[index]['name'].indexOf(company) != -1){
			json['error'] = false;
			json['value'] = companyList[index];
			return json;
		}
	}
	json['error'] = true;
	json['description'] = 'not found company';
	return json;
}

exports.tracking("CJ 대한통운", "610175183562");